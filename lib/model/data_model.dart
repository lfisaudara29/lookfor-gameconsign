class DataModel {
  final int? id;
  final String? img;
  final String? name;
  final String? address;
  final String? area;
  final String? contact;
  final String? desc;

  DataModel(
      {this.id,
      this.img,
      this.name,
      this.address,
      this.area,
      this.contact,
      this.desc}); 
}

const List dataHospitalModel = [
  {
    'id': 1,
    'img': "assets/img/hospital1.jpg",
    'name': "RS. Hermina Depok",
    'address': "Jl. Raya Depok II no.88, Depok, Jawa Barat",
    'area': "Kota Depok",
    'contact': "0123489213523589",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 2,
    'img': "assets/img/hospital2.jpg",
    'name': "RS. Primahusada Depok",
    'address': "Jl. Cinere Raya no.12, Depok, Jawa Barat",
    'area': "Kota Depok",
    'contact': "0142918177513971",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 3,
    'img': "assets/img/hospital3.jpg",
    'name': "RS. Prikasih Pondok Labu",
    'address': "Jl. Pondok Labu no.44, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "07310192472915729",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 4,
    'img': "assets/img/hospital1.jpg",
    'name': "RS. Hermina Jakarta Selatan",
    'address': "Jl. Pondok Indah no.76, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "14217911052147192",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 5,
    'img': "assets/img/hospital2.jpg",
    'name': "RS. Budi Utomo",
    'address': "Jl. Pengasinan blok d no.08, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "41151116163622844",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 6,
    'img': "assets/img/hospital3.jpg",
    'name': "RS. Geo Sehat",
    'address': "Jl. Bogor IV no.12, Bogor, Jawa Barat",
    'area': "Kota Bogor",
    'contact': "61632711735142123",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
];

const List dataRestoModel = [
  {
    'id': 1,
    'img': "assets/img/resto1.jpg",
    'name': "Resto Cangkir Kopi",
    'address': "Jl. Raya Depok II no.88, Depok, Jawa Barat",
    'area': "Kota Depok",
    'contact': "0123489213523589",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 2,
    'img': "assets/img/resto2.jpg",
    'name': "Resto Bakoel 5",
    'address': "Jl. Cinere Raya no.12, Depok, Jawa Barat",
    'area': "Kota Depok",
    'contact': "0142918177513971",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 3,
    'img': "assets/img/resto3.jpg",
    'name': "Resto Serba Serbi",
    'address': "Jl. Pondok Labu no.44, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "07310192472915729",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 4,
    'img': "assets/img/resto1.jpg",
    'name': "Resto Meat & Eat",
    'address': "Jl. Pondok Indah no.76, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "14217911052147192",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 5,
    'img': "assets/img/resto2.jpg",
    'name': "Resto Luxury",
    'address': "Jl. Pengasinan blok d no.08, Jakarta Selatan, Jawa Barat",
    'area': "Kota Jakarta Selatan",
    'contact': "41151116163622844",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
  {
    'id': 6,
    'img': "assets/img/resto3.jpg",
    'name': "Resto Sunda Pride",
    'address': "Jl. Bogor IV no.12, Bogor, Jawa Barat",
    'area': "Kota Bogor",
    'contact': "61632711735142123",
    'desc':
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Blanditiis alias nobis, tempora temporibus molestias expedita cupiditate doloribus maiores inventore omnis quasi autem magni voluptatem consequuntur. Odio facere vero suscipit voluptatem!",
  },
];
