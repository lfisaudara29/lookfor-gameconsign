import 'package:flutter/material.dart' show Color, Colors;

const primary = Color(0xFF047AAC);
const bg = Color(0xFFEBE9E8);
const shadow = Color(0xFFD4D3D2);
const white = Colors.white;
const grey = Colors.grey;