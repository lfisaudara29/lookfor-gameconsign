import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geocoding/geocoding.dart';
import 'package:latlong2/latlong.dart';
import 'package:lookfor/util/colors.dart';
import 'package:lookfor/view_model/position_view_model.dart';
import 'package:open_street_map_search_and_pick/open_street_map_search_and_pick.dart';
import 'package:open_street_map_search_and_pick/widgets/wide_button.dart';
import 'package:http/http.dart' as http;

class MapsPage extends ConsumerStatefulWidget {
  const MapsPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _MapsPageState();
}

class _MapsPageState extends ConsumerState<MapsPage> {
  double? long;
  double? lat;
  MapController _mapController = MapController();
  final TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _initial();
  }

  Future<void> _initial() async {
    Future(() {
      final viewModel = ref.read(positionViewModel);
      viewModel.checkPermission(context);
      viewModel.getPosition();
      final latlong = LatLng(ref.watch(positionViewModel).position.latitude,
          ref.watch(positionViewModel).position.longitude);

      viewModel.setPosition(latlong);
      _searchController.text = ref.watch(positionViewModel).address;

      _onMapMove();
    });
  }

  void _onMapMove() async {
    _mapController.mapEventStream.listen((event) async {
      if (event is MapEventMoveEnd) {
        await placemarkFromCoordinates(
                event.center.latitude, event.center.longitude)
            .then((List<Placemark> placemarks) {
          Placemark place = placemarks[0];
          _searchController.text =
              '${place.street}, ${place.subLocality}, ${place.subAdministrativeArea}, ${place.postalCode}';
        }).catchError((e) {
          debugPrint(e);
        });
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer(
        builder: (context, ref, child) {
          final viewModel = ref.watch(positionViewModel);
          final position = viewModel.positionNow;
          final address = viewModel.address;
          return Stack(
            children: [
              Positioned.fill(
                  child: FlutterMap(
                options: MapOptions(
                  center: LatLng(position.latitude, position.longitude),
                  zoom: 18.0,
                  maxZoom: 20,
                  minZoom: 6,
                ),
                mapController: _mapController,
                children: [
                  TileLayer(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c'],
                  ),
                ],
              )),
              Positioned(
                  top: 40,
                  left: 10,
                  right: 10,
                  child: IgnorePointer(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(color: grey, blurRadius: 2, spreadRadius: 1)
                        ],
                      ),
                      child: Text(
                        _searchController.text,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )),
              Positioned.fill(
                  child: IgnorePointer(
                child: Center(
                  child: Icon(Icons.location_pin, size: 50, color: primary),
                ),
              )),
              Positioned(
                  bottom: 60,
                  right: 5,
                  child: FloatingActionButton(
                    heroTag: 'btn3',
                    backgroundColor: primary,
                    onPressed: () {
                      _mapController.move(
                          LatLng(position.latitude, position.longitude),
                          _mapController.zoom);
                      setState(() {
                        _searchController.text = address;
                      });
                      // setNameCurrentPos();
                    },
                    child: Icon(Icons.my_location),
                  )),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: WideButton("Set", onPressed: () async {
                      pickData().then((value) {
                        (pickedData) {
                          final viewModel = ref.watch(positionViewModel);

                          final latlong = LatLng(pickedData.latLong.latitude,
                              pickedData.latLong.longitude);

                          viewModel.setTitle(latlong);
                          viewModel.setPosition(latlong);

                          Navigator.pop(context);
                        }(value);
                      });
                    }, backgroundcolor: primary),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  Future<PickedData> pickData() async {
    LatLong center = LatLong(
        _mapController.center.latitude, _mapController.center.longitude);
    var client = http.Client();
    String url =
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=${_mapController.center.latitude}&lon=${_mapController.center.longitude}&zoom=18&addressdetails=1';

    var response = await client.post(Uri.parse(url));
    var decodedResponse =
        jsonDecode(utf8.decode(response.bodyBytes)) as Map<dynamic, dynamic>;
    String displayName = decodedResponse['display_name'];
    return PickedData(center, displayName);
  }
}
