import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/util/colors.dart';
import 'package:lookfor/view/home/home_page.dart';
import 'package:lookfor/view_model/position_view_model.dart';

class SplashscreenPage extends ConsumerStatefulWidget {
  const SplashscreenPage({Key? key}) : super(key: key);

  @override
  ConsumerState<SplashscreenPage> createState() => _SplashscreenPageState();
}

class _SplashscreenPageState extends ConsumerState<SplashscreenPage> {
  bool isLocationOn = false;

  @override
  void initState() {
    super.initState();
    _initial();
    _countdownNavigation();
  }

  Future<void> _initial() async {
    Future(() {
      final viewModel = ref.read(positionViewModel);
      viewModel.getGeolocationPosition();
    });
  }

  void _countdownNavigation() {
    Future.delayed(Duration(milliseconds: 3000), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: white,
      child: SplashscreenImg(),
    );
  }
}

class SplashscreenImg extends StatelessWidget {
  const SplashscreenImg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: SizedBox(
        width: double.infinity,
        child: Image.asset('assets/img/logo.png'),
      ),
    );
  }
}
