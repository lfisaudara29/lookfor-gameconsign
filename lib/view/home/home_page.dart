import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/util/colors.dart';
import 'package:lookfor/view/home/home_list_place.dart';
import 'package:lookfor/view/home/widget/home_location.dart';
import 'package:lookfor/view_model/position_view_model.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  ConsumerState<HomePage> createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  @override
  void initState() {
    super.initState();
    _initial();
  }

  Future<void> _initial() async {
    Future(() {
      final viewModel = ref.read(positionViewModel);
      viewModel.checkPermission(context);
      viewModel.getPosition();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bg,
      body: Consumer(
        builder: (context, ref, child) {
          return Stack(
            children: [
              HomeContent(),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    color: bg,
                    padding: EdgeInsets.fromLTRB(16, 46, 16, 0),
                    child: HomeLocation(),
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}

class HomeContent extends StatefulWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  State<HomeContent> createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> {
  int _selectedIndex = 0;

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentscreen = HomeListHospital();

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;
    return Stack(
      children: [
        PageStorage(
          bucket: bucket,
          child: Padding(
            padding: EdgeInsets.fromLTRB(16, 140, 16, 0),
            child: currentscreen,
          ),
        ),
        Container(
          color: bg,
          padding: EdgeInsets.fromLTRB(16, 105, 16, 6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                minWidth: _media.width / 2.4,
                color: _selectedIndex == 0 ? primary : white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                onPressed: () {
                  setState(() {
                    currentscreen = HomeListHospital();
                    _selectedIndex = 0;
                  });
                },
                child: Text(
                  'Hospital',
                  style: TextStyle(
                      color: _selectedIndex == 0 ? white : grey,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                ),
              ),
              SizedBox(width: 12),
              MaterialButton(
                minWidth: _media.width / 2.4,
                color: _selectedIndex == 1 ? primary : white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                onPressed: () {
                  setState(() {
                    currentscreen = HomeListRestaurant();
                    _selectedIndex = 1;
                  });
                },
                child: Text(
                  'Restaurant',
                  style: TextStyle(
                      color: _selectedIndex == 1 ? white : grey,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
