import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/util/colors.dart';
import 'package:lookfor/util/finit_state.dart';
import 'package:lookfor/view/maps/maps_page.dart';
import 'package:lookfor/view_model/position_view_model.dart';

class HomeLocation extends ConsumerWidget {
  const HomeLocation({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final viewModel = ref.watch(positionViewModel);
    final address = viewModel.address;
    switch (viewModel.actionState) {
      case StateAction.none:
        return Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(50),
              boxShadow: [
                BoxShadow(color: shadow, blurRadius: 2, spreadRadius: 1)
              ]),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MapsPage()));
            },
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Your Location',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(height: 2),
                      Text(
                        address,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: const Icon(
                      Icons.edit_location_alt_rounded,
                      color: primary,
                      size: 22,
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      case StateAction.loading:
        return Container(
            height: 10,
            width: 10,
            child: Center(child: CircularProgressIndicator()));
      case StateAction.error:
        return const SizedBox.shrink();
    }
  }
}
