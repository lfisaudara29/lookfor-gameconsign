import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/util/colors.dart';
import 'package:lookfor/view/home/widget/card_place.dart';
import 'package:lookfor/view/home/widget/detail_list_page.dart';
import 'package:lookfor/view_model/hospital_view_model.dart';
import 'package:lookfor/view_model/position_view_model.dart';
import 'package:lookfor/view_model/resto_view_model.dart';

class HomeListHospital extends ConsumerStatefulWidget {
  const HomeListHospital({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _HomeListHospitalState();
}

class _HomeListHospitalState extends ConsumerState<HomeListHospital> {
  @override
  void initState() {
    super.initState();
    _initial();
  }

  Future<void> _initial() async {
    Future(() {
      final viewModel = ref.read(hospitalViewModel);
      viewModel.getDataHospital(ref.watch(positionViewModel).area);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final viewModel = ref.watch(hospitalViewModel);
      final dataHospitals = viewModel.dataHospital;
      return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: dataHospitals.length,
        itemBuilder: (context, index) {
          final dataHospital = dataHospitals.elementAt(index);
          return Column(
            children: [
              CardPlace(
                onTap: () => showModalBottomSheet(
                    context: context,
                    backgroundColor: white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(18),
                      topRight: Radius.circular(18),
                    )),
                    builder: (builder) {
                      return DetailListPage(
                        dataModel: dataHospital,
                      );
                    }),
                img: dataHospital.img!,
                name: dataHospital.name!,
                address: dataHospital.address!,
              ),
              const SizedBox(height: 12),
            ],
          );
        },
      );
    });
  }
}

class HomeListRestaurant extends ConsumerStatefulWidget {
  const HomeListRestaurant({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _HomeListRestaurantState();
}

class _HomeListRestaurantState extends ConsumerState<HomeListRestaurant> {
  @override
  void initState() {
    super.initState();
    _initial();
  }

  Future<void> _initial() async {
    Future(() {
      final viewModel = ref.read(restoViewModel);
      viewModel.getDataResto(ref.watch(positionViewModel).area);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final viewModel = ref.watch(restoViewModel);
      final dataRestos = viewModel.dataResto;
      return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: dataRestos.length,
        itemBuilder: (context, index) {
          final dataResto = dataRestos.elementAt(index);
          return Column(
            children: [
              CardPlace(
                onTap: () => showModalBottomSheet(
                    context: context,
                    backgroundColor: white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(18),
                      topRight: Radius.circular(18),
                    )),
                    builder: (builder) {
                      return DetailListPage(
                        dataModel: dataResto,
                      );
                    }),
                img: dataResto.img!,
                name: dataResto.name!,
                address: dataResto.address!,
              ),
              const SizedBox(height: 12),
            ],
          );
        },
      );
    });
  }
}
