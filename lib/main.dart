import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/app/app.dart';

void main() {
  runApp(ProviderScope(child: App()));
}
