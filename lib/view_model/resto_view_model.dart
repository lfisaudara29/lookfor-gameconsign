import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/model/data_model.dart';
import 'package:lookfor/util/finit_state.dart';

class RestoViewModel extends ChangeNotifier with FiniteState {
  List<DataModel> _dataResto = [];
  List<DataModel> get dataResto => _dataResto;

  Future<void> getDataResto(String area) async {
    setStateAction(StateAction.loading);
    try {
      final result = dataRestoModel
          .map(
            (data) => DataModel(
              id: data['id'],
              img: data['img'],
              name: data['name'],
              address: data['address'],
              area: data['area'],
              contact: data['contact'],
              desc: data['desc'],
            ),
          )
          .toList();

      _dataResto =
          result.where((result) => result.area!.contains(area)).toList();

      notifyListeners();
      setStateAction(StateAction.none);
    } catch (e) {
      setStateAction(StateAction.error);
    }
  }
}

final restoViewModel =
    ChangeNotifierProvider<RestoViewModel>((ref) => RestoViewModel());
