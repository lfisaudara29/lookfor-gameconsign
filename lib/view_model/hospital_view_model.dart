import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lookfor/model/data_model.dart';
import 'package:lookfor/util/finit_state.dart';

class HospitalViewModel extends ChangeNotifier with FiniteState {
  List<DataModel> _dataHospital = [];
  List<DataModel> get dataHospital => _dataHospital;

  Future<void> getDataHospital(String area) async {
    setStateAction(StateAction.loading);
    try {
      final result = dataHospitalModel
          .map(
            (data) => DataModel(
              id: data['id'],
              img: data['img'],
              name: data['name'],
              address: data['address'],
              area: data['area'],
              contact: data['contact'],
              desc: data['desc'],
            ),
          )
          .toList();

      _dataHospital =
          result.where((result) => result.area!.contains(area)).toList();

      notifyListeners();
      setStateAction(StateAction.none);
    } catch (e) {
      setStateAction(StateAction.error);
    }
  }
}

final hospitalViewModel =
    ChangeNotifierProvider<HospitalViewModel>((ref) => HospitalViewModel());
