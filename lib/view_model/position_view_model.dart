import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:lookfor/service/position_service.dart';
import 'package:lookfor/util/finit_state.dart';

class PositionViewModel extends ChangeNotifier
    with FiniteState
    implements GetPosition {
  final _positionService = PositionService();

  Position get position => _position!;
  Position? _position;

  LatLng get positionNow => _positionNow!;
  LatLng? _positionNow;

  String get address => _address;
  String _address = "";

  String get area => _area;
  String _area = "";

  void setTitle(LatLng latLng) async {
    await placemarkFromCoordinates(latLng.latitude, latLng.longitude)
        .then((List<Placemark> placemarks) {
      Placemark place = placemarks[0];
      _address =
          '${place.street}, ${place.subLocality}, ${place.subAdministrativeArea}, ${place.postalCode}';
      _area = '${place.subAdministrativeArea}';
    }).catchError((e) {
      debugPrint(e);
    });
    notifyListeners();
  }

  void setPosition(LatLng latLng) {
    _positionNow = latLng;
    notifyListeners();
  }

  Future<void> getPosition() async {
    setStateAction(StateAction.loading);
    try {
      _position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      await placemarkFromCoordinates(_position!.latitude, _position!.longitude)
          .then((List<Placemark> placemarks) {
        Placemark place = placemarks[0];
        _address =
            '${place.street}, ${place.subLocality}, ${place.subAdministrativeArea}, ${place.postalCode}';
        _area = '${place.subAdministrativeArea}';
      }).catchError((e) {
        debugPrint(e);
      });
      notifyListeners();
      setStateAction(StateAction.none);
    } catch (e) {
      setStateAction(StateAction.error);
    }
  }

  @override
  Future<Position> getGeolocationPosition() {
    final result = _positionService.getGeolocationPosition();
    notifyListeners();
    return result;
  }

  @override
  Future<void> checkPermission(BuildContext context) {
    final result = _positionService.checkPermission(context);
    notifyListeners();
    return result;
  }
}

final positionViewModel =
    ChangeNotifierProvider<PositionViewModel>((ref) => PositionViewModel());
