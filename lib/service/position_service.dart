import 'package:android_intent_plus/android_intent.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:permission_handler/permission_handler.dart';

abstract class GetPosition {
  // Future getPosition();
  Future<Position> getGeolocationPosition();
  Future<void> checkPermission(BuildContext context);
}

class PositionService implements GetPosition {
  Position? _position;
  Position get position => _position!;

  String? _address;
  String get address => _address!;

  bool _isLocationOn = false;
  bool get isLocationOn => _isLocationOn;

  bool? serviceEnabled;
  LocationPermission? permission;

  bool _isGPS = false;
  bool get isGPS => _isGPS;

  @override
  Future<Position> getGeolocationPosition() async {
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled!) {
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      print("isLocation : $isLocationOn");
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    _isLocationOn = true;
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  @override
  Future<void> checkPermission(BuildContext context) async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled ||
        await Permission.locationAlways.serviceStatus.isEnabled) {
      if (_isGPS == true) {
        _isGPS = false;
      }
      checkPermission(context);
    } else {
      checkPermission(context);

      if (_isGPS == false) {
        CoolAlert.show(
          showCancelBtn: true,
          context: context,
          type: CoolAlertType.warning,
          title: 'Requires Location Permission',
          text: 'This app requires location permission to work properly.',
          confirmBtnText: 'ACTIVATE',
          onConfirmBtnTap: () async {
            Navigator.pop(context);

            final AndroidIntent intent = new AndroidIntent(
              action: 'android.settings.LOCATION_SOURCE_SETTINGS',
            );
            await intent.launch();
          },
        );
        _isGPS = true;
      }
    }
  }
}
